#!/bin/bash
# Nome : glpibackup.sh
# Versão: 1.0
# Autor : Marcos Ferreira da Rocha
# Email : marcos.fr.rocha@gmail.com
# telegram: @ferreirarocha
# Site : alfabe.ch

# Descrição :  O script glpibackup faz  o bacup do banco de dados e diretório do glpi
# Possui as opções  full (backup completo) e lite (backup dos dados e  pics, files e plugins do glpi).
# O processo de autenticação do banco de dados usa o unix_socket evitando a exposição da senha, os logs podem ser consultados diretamente no systemd
# journalctl -u glpiBackup  --since today

#VARIAVEIS
data=$(date +"%Y-%m-%d_%H-%M")
dirglpi=/var/www/html/vhost
bkpdir=/var/backups/glpi
logdir=/var/log/backupglpi

# CRIANDO DIRETÓRIO DE BACKUP
if [ ! -d $bkpdir ]; then
    mkdir  -m 770 -p $bkpdir/
    chown .backup $bkpdir -Rf
fi

# CRIANDO DIRETÓRIO DE LOGS
if [ ! -d  $logdir ]; then
    mkdir $logdir
    chmod  775 $logdir
    chown  .backup $logdir -Rf
fi

cd $dirglpi

case $1 in

 lite)

    ## Backup arquivos glpi
    zip -r $bkpdir/$data-bkpglpi-lite.zip $dirglpi/{plugins,pics}

    zip -ur $bkpdir/$data-bkpglpi-lite.zip /var/lib/glpi/

    ## Backup banco glpi
    mysqldump -u backupadmin  glpi >  /var/lib/glpi/_dumps/$data.sql

    zip -ur $bkpdir/$data-bkpglpi-lite.zip /var/lib/glpi/_dumps/$data.sql

    rm /var/lib/glpi/_dumps/$data.sql

    ## Backup Systemd Tarefas
    zip -ur $bkpdir/$data-bkpglpi-lite.zip /usr/lib/systemd/system/glpi*

    ## Backup do script de backup
    zip -ur $bkpdir/$data-bkpglpi-lite.zip /opt/backup-glpi.sh

    ## Backup config http
    zip -ur $bkpdir/$data-bkpglpi-lite.zip /etc/apache2/

    ## Backup config PHP
    zip -ur $bkpdir/$data-bkpglpi-lite.zip /etc/php/7.4/apache2/

    ## Backup config Rclone
    zip -ur $bkpdir/$data-bkpglpi-lite.zip /home/backupadmin/.config/rclone/

    zip -ur $bkpdir/$data-bkpglpi-lite.zip /usr/bin/rclone

    ## Removendo versões antigas
    find  /var/backups/glpi/* -type f -mtime +10  -exec rm -rf {} \;

    ## Enviar Backup para OneDrive
    #rclone  copy -v /var/backups/glpi/  backup-glpi:Backups_GLPI

    ;;

 full)
    ## Use essa área caso queira implementar backup completo
   ;;
 *)

    echo "Função não  implementada"
    ;;
esac
