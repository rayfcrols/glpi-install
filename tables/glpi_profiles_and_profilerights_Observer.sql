-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `glpi_profiles`
--

--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (2,'Observer','central',0,1,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','[]',NULL,'','[]',0,0,0,0,NULL,'[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (45,2,'state',0),(46,2,'taskcategory',0),(60,2,'netpoint',0),(68,2,'computer',33),(69,2,'monitor',33),(70,2,'software',33),(71,2,'networking',33),(72,2,'internet',1),(73,2,'printer',33),(74,2,'peripheral',33),(75,2,'cartridge',33),(76,2,'consumable',33),(77,2,'phone',33),(79,2,'contact_enterprise',33),(80,2,'document',33),(81,2,'contract',33),(82,2,'infocom',1),(83,2,'knowbase',10241),(84,2,'reservation',1025),(85,2,'reports',1),(86,2,'dropdown',0),(87,2,'device',0),(88,2,'typedoc',1),(89,2,'link',1),(90,2,'config',0),(91,2,'rule_ticket',0),(92,2,'rule_import',0),(93,2,'rule_ldap',0),(94,2,'rule_softwarecategories',0),(95,2,'search_config',1024),(98,2,'profile',0),(99,2,'user',2049),(100,2,'group',33),(101,2,'entity',0),(102,2,'transfer',0),(103,2,'logs',0),(104,2,'reminder_public',1),(105,2,'rssfeed_public',1),(106,2,'bookmark_public',0),(107,2,'backup',0),(108,2,'ticket',168989),(109,2,'followup',5),(110,2,'task',1),(113,2,'planning',1),(116,2,'statistic',1),(117,2,'password_update',1),(118,2,'show_group_hardware',0),(119,2,'rule_dictionnary_software',0),(120,2,'rule_dictionnary_dropdown',0),(121,2,'budget',33),(122,2,'notification',0),(123,2,'rule_mailcollector',0),(126,2,'calendar',0),(127,2,'slm',0),(128,2,'rule_dictionnary_printer',0),(129,2,'problem',1057),(133,2,'itiltemplate',0),(134,2,'ticketrecurrent',0),(135,2,'ticketcost',1),(138,2,'ticketvalidation',15376),(202,2,'knowbasecategory',0),(207,2,'changevalidation',1044),(237,2,'location',0),(254,2,'projecttask',1025),(267,2,'solutiontemplate',0),(273,2,'itilcategory',0),(361,2,'queuednotification',0),(379,2,'domain',0),(394,2,'project',1025),(489,2,'change',1057),(563,2,'license',33),(571,2,'line',33),(579,2,'lineoperator',33),(587,2,'devicesimcard_pinpuk',1),(595,2,'certificate',33),(603,2,'datacenter',1),(612,2,'personalization',3),(620,2,'rule_asset',0),(627,2,'global_validation',0),(634,2,'cluster',1),(642,2,'externalevent',1),(650,2,'dashboard',0),(658,2,'appliance',1),(667,2,'plugin_fusioninventory_menu',0),(675,2,'plugin_fusioninventory_agent',0),(683,2,'plugin_fusioninventory_remotecontrol',0),(691,2,'plugin_fusioninventory_configuration',0),(699,2,'plugin_fusioninventory_task',0),(707,2,'plugin_fusioninventory_wol',0),(715,2,'plugin_fusioninventory_group',0),(723,2,'plugin_fusioninventory_collect',0),(731,2,'plugin_fusioninventory_iprange',0),(739,2,'plugin_fusioninventory_credential',0),(747,2,'plugin_fusioninventory_credentialip',0),(755,2,'plugin_fusioninventory_esx',0),(763,2,'plugin_fusioninventory_configsecurity',0),(771,2,'plugin_fusioninventory_networkequipment',0),(779,2,'plugin_fusioninventory_printer',0),(787,2,'plugin_fusioninventory_unmanaged',0),(795,2,'plugin_fusioninventory_importxml',0),(803,2,'plugin_fusioninventory_reportprinter',0),(811,2,'plugin_fusioninventory_reportnetworkequipment',0),(819,2,'plugin_fusioninventory_lock',0),(827,2,'plugin_fusioninventory_ruleimport',0),(835,2,'plugin_fusioninventory_ruleentity',0),(843,2,'plugin_fusioninventory_rulelocation',0),(851,2,'plugin_fusioninventory_blacklist',0),(859,2,'plugin_fusioninventory_rulecollect',0),(867,2,'plugin_fusioninventory_ignoredimportdevice',0),(875,2,'plugin_fusioninventory_package',0),(883,2,'plugin_fusioninventory_userinteractiontemplate',0),(891,2,'plugin_fusioninventory_deploymirror',0),(899,2,'plugin_fusioninventory_selfpackage',0),(909,2,'plugin_moreticket',0),(917,2,'plugin_moreticket_justification',0),(929,2,'plugin_pdf',0),(938,2,'plugin_satisfaction',0),(947,2,'plugin_tag_tag',0),(955,2,'plugin_tasklists',0),(963,2,'plugin_tasklists_see_all',0),(971,2,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:15:47
