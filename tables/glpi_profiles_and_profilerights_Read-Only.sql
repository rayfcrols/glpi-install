-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (8,'Read-Only','central',0,0,'[]','{\"1\":{\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0},\"2\":{\"1\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0},\"3\":{\"1\":0,\"2\":0,\"4\":0,\"5\":0,\"6\":0},\"4\":{\"1\":0,\"2\":0,\"3\":0,\"5\":0,\"6\":0},\"5\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"6\":0},\"6\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0}}',NULL,'This profile defines read-only access. It is used when objects are locked. It can also be used to give to users rights to unlock objects.','{\"1\":{\"7\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"8\":0,\"6\":0},\"7\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"8\":0,\"6\":0},\"2\":{\"1\":0,\"7\":0,\"3\":0,\"4\":0,\"5\":0,\"8\":0,\"6\":0},\"3\":{\"1\":0,\"7\":0,\"2\":0,\"4\":0,\"5\":0,\"8\":0,\"6\":0},\"4\":{\"1\":0,\"7\":0,\"2\":0,\"3\":0,\"5\":0,\"8\":0,\"6\":0},\"5\":{\"1\":0,\"7\":0,\"2\":0,\"3\":0,\"4\":0,\"8\":0,\"6\":0},\"8\":{\"1\":0,\"7\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0},\"6\":{\"1\":0,\"7\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"8\":0}}',0,0,0,0,'{\"1\":{\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"9\":{\"1\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"10\":{\"1\":0,\"9\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"7\":{\"1\":0,\"9\":0,\"10\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"4\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"11\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"12\":0,\"5\":0,\"8\":0,\"6\":0},\"12\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"5\":0,\"8\":0,\"6\":0},\"5\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"8\":0,\"6\":0},\"8\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"6\":0},\"6\":{\"1\":0,\"9\":0,\"10\":0,\"7\":0,\"4\":0,\"11\":0,\"12\":0,\"5\":0,\"8\":0}}','[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (491,8,'backup',1),(492,8,'bookmark_public',1),(493,8,'budget',33),(494,8,'calendar',1),(495,8,'cartridge',33),(496,8,'change',1057),(497,8,'changevalidation',0),(498,8,'computer',33),(499,8,'config',1),(500,8,'consumable',33),(501,8,'contact_enterprise',33),(502,8,'contract',33),(503,8,'device',1),(504,8,'document',33),(505,8,'domain',1),(506,8,'dropdown',1),(507,8,'entity',33),(508,8,'followup',8193),(509,8,'global_validation',0),(510,8,'group',33),(511,8,'infocom',1),(512,8,'internet',1),(513,8,'itilcategory',1),(514,8,'knowbase',10241),(515,8,'knowbasecategory',1),(516,8,'link',1),(517,8,'location',1),(518,8,'logs',1),(519,8,'monitor',33),(520,8,'netpoint',1),(521,8,'networking',33),(522,8,'notification',1),(523,8,'password_update',0),(524,8,'peripheral',33),(525,8,'phone',33),(526,8,'planning',3073),(527,8,'printer',33),(528,8,'problem',1057),(529,8,'profile',1),(530,8,'project',1057),(531,8,'projecttask',33),(532,8,'queuednotification',1),(533,8,'reminder_public',1),(534,8,'reports',1),(535,8,'reservation',1),(536,8,'rssfeed_public',1),(537,8,'rule_dictionnary_dropdown',1),(538,8,'rule_dictionnary_printer',1),(539,8,'rule_dictionnary_software',1),(540,8,'rule_import',1),(541,8,'rule_ldap',1),(542,8,'rule_mailcollector',1),(543,8,'rule_softwarecategories',1),(544,8,'rule_ticket',1),(545,8,'search_config',0),(546,8,'show_group_hardware',1),(547,8,'slm',1),(548,8,'software',33),(549,8,'solutiontemplate',1),(550,8,'state',1),(551,8,'statistic',1),(552,8,'task',8193),(553,8,'taskcategory',1),(554,8,'ticket',138241),(555,8,'ticketcost',1),(556,8,'ticketrecurrent',1),(557,8,'itiltemplate',1),(558,8,'ticketvalidation',0),(559,8,'transfer',1),(560,8,'typedoc',1),(561,8,'user',1),(569,8,'license',33),(577,8,'line',33),(585,8,'lineoperator',1),(593,8,'devicesimcard_pinpuk',1),(601,8,'certificate',33),(609,8,'datacenter',1),(618,8,'personalization',3),(625,8,'rule_asset',0),(640,8,'cluster',1),(648,8,'externalevent',1),(656,8,'dashboard',0),(664,8,'appliance',1),(673,8,'plugin_fusioninventory_menu',0),(681,8,'plugin_fusioninventory_agent',0),(689,8,'plugin_fusioninventory_remotecontrol',0),(697,8,'plugin_fusioninventory_configuration',0),(705,8,'plugin_fusioninventory_task',0),(713,8,'plugin_fusioninventory_wol',0),(721,8,'plugin_fusioninventory_group',0),(729,8,'plugin_fusioninventory_collect',0),(737,8,'plugin_fusioninventory_iprange',0),(745,8,'plugin_fusioninventory_credential',0),(753,8,'plugin_fusioninventory_credentialip',0),(761,8,'plugin_fusioninventory_esx',0),(769,8,'plugin_fusioninventory_configsecurity',0),(777,8,'plugin_fusioninventory_networkequipment',0),(785,8,'plugin_fusioninventory_printer',0),(793,8,'plugin_fusioninventory_unmanaged',0),(801,8,'plugin_fusioninventory_importxml',0),(809,8,'plugin_fusioninventory_reportprinter',0),(817,8,'plugin_fusioninventory_reportnetworkequipment',0),(825,8,'plugin_fusioninventory_lock',0),(833,8,'plugin_fusioninventory_ruleimport',0),(841,8,'plugin_fusioninventory_ruleentity',0),(849,8,'plugin_fusioninventory_rulelocation',0),(857,8,'plugin_fusioninventory_blacklist',0),(865,8,'plugin_fusioninventory_rulecollect',0),(873,8,'plugin_fusioninventory_ignoredimportdevice',0),(881,8,'plugin_fusioninventory_package',0),(889,8,'plugin_fusioninventory_userinteractiontemplate',0),(897,8,'plugin_fusioninventory_deploymirror',0),(905,8,'plugin_fusioninventory_selfpackage',0),(915,8,'plugin_moreticket',0),(923,8,'plugin_moreticket_justification',0),(935,8,'plugin_pdf',0),(944,8,'plugin_satisfaction',0),(953,8,'plugin_tag_tag',0),(961,8,'plugin_tasklists',0),(969,8,'plugin_tasklists_see_all',0),(977,8,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:18:24
