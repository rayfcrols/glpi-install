-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: glpi
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Dumping data for table `glpi_profiles`
--

LOCK TABLES `glpi_profiles` WRITE;
/*!40000 ALTER TABLE `glpi_profiles` DISABLE KEYS */;
INSERT INTO `glpi_profiles` VALUES (1,'Self-Service','helpdesk',1,1,'[\"Computer\",\"Monitor\",\"NetworkEquipment\",\"Peripheral\",\"Phone\",\"Printer\",\"Software\", \"DCRoom\", \"Rack\", \"Enclosure\"]','{\"1\":{\"2\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0},\"2\":{\"1\":0,\"3\":0,\"4\":0,\"5\":0,\"6\":0},\"3\":{\"1\":0,\"2\":0,\"4\":0,\"5\":0,\"6\":0},\"4\":{\"1\":0,\"2\":0,\"3\":0,\"5\":0,\"6\":0},\"5\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0},\"6\":{\"1\":0,\"2\":0,\"3\":0,\"4\":0,\"5\":0}}',NULL,'','[]',0,0,0,0,NULL,'[]',NULL);
/*!40000 ALTER TABLE `glpi_profiles` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `glpi_profilerights`
--

LOCK TABLES `glpi_profilerights` WRITE;
/*!40000 ALTER TABLE `glpi_profilerights` DISABLE KEYS */;
INSERT INTO `glpi_profilerights` VALUES (1,1,'computer',0),(2,1,'monitor',0),(3,1,'software',0),(4,1,'networking',0),(5,1,'internet',0),(6,1,'printer',0),(7,1,'peripheral',0),(8,1,'cartridge',0),(9,1,'consumable',0),(10,1,'phone',0),(12,1,'contact_enterprise',0),(13,1,'document',0),(14,1,'contract',0),(15,1,'infocom',0),(16,1,'knowbase',2048),(17,1,'reservation',1024),(18,1,'reports',0),(19,1,'dropdown',0),(20,1,'device',0),(21,1,'typedoc',0),(22,1,'link',0),(23,1,'config',0),(24,1,'rule_ticket',0),(25,1,'rule_import',0),(26,1,'rule_ldap',0),(27,1,'rule_softwarecategories',0),(28,1,'search_config',0),(31,1,'profile',0),(32,1,'user',0),(33,1,'group',0),(34,1,'entity',0),(35,1,'transfer',0),(36,1,'logs',0),(37,1,'reminder_public',1),(38,1,'rssfeed_public',1),(39,1,'bookmark_public',0),(40,1,'backup',0),(41,1,'ticket',5),(42,1,'followup',5),(43,1,'task',1),(44,1,'planning',0),(47,1,'statistic',0),(48,1,'password_update',1),(49,1,'show_group_hardware',0),(50,1,'rule_dictionnary_software',0),(51,1,'rule_dictionnary_dropdown',0),(52,1,'budget',0),(53,1,'notification',0),(54,1,'rule_mailcollector',0),(56,1,'calendar',0),(57,1,'slm',0),(58,1,'rule_dictionnary_printer',0),(59,1,'problem',0),(63,1,'itiltemplate',0),(64,1,'ticketrecurrent',0),(65,1,'ticketcost',0),(67,1,'ticketvalidation',0),(114,1,'state',0),(115,1,'taskcategory',0),(130,1,'netpoint',0),(253,1,'projecttask',0),(266,1,'solutiontemplate',0),(272,1,'knowbasecategory',0),(278,1,'changevalidation',0),(308,1,'location',0),(344,1,'itilcategory',0),(393,1,'project',0),(432,1,'queuednotification',0),(450,1,'domain',0),(488,1,'change',0),(562,1,'license',0),(570,1,'line',0),(578,1,'lineoperator',0),(586,1,'devicesimcard_pinpuk',0),(594,1,'certificate',0),(602,1,'datacenter',0),(611,1,'personalization',3),(619,1,'rule_asset',0),(626,1,'global_validation',0),(633,1,'cluster',0),(641,1,'externalevent',0),(649,1,'dashboard',0),(657,1,'appliance',0),(674,1,'plugin_fusioninventory_menu',0),(682,1,'plugin_fusioninventory_agent',0),(690,1,'plugin_fusioninventory_remotecontrol',0),(698,1,'plugin_fusioninventory_configuration',0),(706,1,'plugin_fusioninventory_task',0),(714,1,'plugin_fusioninventory_wol',0),(722,1,'plugin_fusioninventory_group',0),(730,1,'plugin_fusioninventory_collect',0),(738,1,'plugin_fusioninventory_iprange',0),(746,1,'plugin_fusioninventory_credential',0),(754,1,'plugin_fusioninventory_credentialip',0),(762,1,'plugin_fusioninventory_esx',0),(770,1,'plugin_fusioninventory_configsecurity',0),(778,1,'plugin_fusioninventory_networkequipment',0),(786,1,'plugin_fusioninventory_printer',0),(794,1,'plugin_fusioninventory_unmanaged',0),(802,1,'plugin_fusioninventory_importxml',0),(810,1,'plugin_fusioninventory_reportprinter',0),(818,1,'plugin_fusioninventory_reportnetworkequipment',0),(826,1,'plugin_fusioninventory_lock',0),(834,1,'plugin_fusioninventory_ruleimport',0),(842,1,'plugin_fusioninventory_ruleentity',0),(850,1,'plugin_fusioninventory_rulelocation',0),(858,1,'plugin_fusioninventory_blacklist',0),(866,1,'plugin_fusioninventory_rulecollect',0),(874,1,'plugin_fusioninventory_ignoredimportdevice',0),(882,1,'plugin_fusioninventory_package',0),(890,1,'plugin_fusioninventory_userinteractiontemplate',0),(898,1,'plugin_fusioninventory_deploymirror',0),(906,1,'plugin_fusioninventory_selfpackage',0),(916,1,'plugin_moreticket',0),(924,1,'plugin_moreticket_justification',0),(936,1,'plugin_pdf',0),(945,1,'plugin_satisfaction',0),(954,1,'plugin_tag_tag',0),(962,1,'plugin_tasklists',0),(970,1,'plugin_tasklists_see_all',0),(978,1,'plugin_tasklists_config',0);
/*!40000 ALTER TABLE `glpi_profilerights` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-20 15:15:03
